# todo angular application

This is the fullstack boilerplate for MeteorJs.

## Requirements

* [NodeJs](http://nodejs.org) >= 6.x 
* [mongodb](http://mongodb.org)

## Install

```sh
$ git clone https://gitlab.com/tranthanh_95/notes_management.git
$ npm install
```

then

```sh
$ meteor run
```

Then visit [http://localhost:3000/](http://localhost:3000/)

## License

MIT